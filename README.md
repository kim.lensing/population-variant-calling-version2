# Population level variant calling

## First follow the instructions here

[Step by step guide on how to use my pipelines](https://carolinapb.github.io/2021-06-23-how-to-run-my-pipelines/)  
Click [here](https://github.com/CarolinaPB/snakemake-template/blob/master/Short%20introduction%20to%20Snakemake.pdf) for an introduction to Snakemake

#### Copy the pipeline:
Copy the pipeline to your directory, where you would run the pipeline.
```
cp -r /lustre/nobackup/WUR/ABGC/shared/pipelines_version2/population-variant-calling-version2 <directory where you want to save it to>
```

### Install conda if it is not yet installed
See installation instructions below. 

#### Activate environment:
Since the pipelines can take a while to run, it’s best if you use a screen session. By using a screen session, Snakemake stays “active” in the shell while it’s running, there’s no risk of the connection going down and Snakemake stopping. `screen -S <name of session>` to start a new screen session and `screen -r <name of session` to reattach to the screen session. In the screen session activate the conda environment.

```
conda activate /lustre/nobackup/WUR/ABGC/shared/pipelines_version2/envs/population-variant-calling-version2
```

#### Edit config.yaml with the paths to your files
Configure your paths, but keep the variable names that are already in the config file. See below for more details.

**Only Ensembl GFF3 files are supporte by bcftools csq.** 
Otherwise change the rule all, so it stops after freebayes and remove the temp() behind the output of freebayes in the snakefile. 

#### Run the pipeline on the cluster:
First test the pipeline with a dry run: `snakemake -np`. This will show you the steps and commands that will be executed. Check the commands and file names to see if there’s any mistake. If all looks ok, you can now run your pipeline


To run the pipeline, run the following command:
```
snakemake -j 8 --cluster-config cluster.yaml --cluster "sbatch --mem={cluster.mem} --time {cluster.time} --cpus-per-task {cluster.threads} --job-name={cluster.name} --output={cluster.output} --error={cluster.error}"

```

## ABOUT

This is a pipeline that takes short reads aligned to a genome (in `.bam` format) and performs population level variant calling with `Freebayes`. It uses BCFtools csq to annotate the resulting VCF, calculates statistics, and calculates and plots a PCA.

It was developed to work with the results of [this population mapping pipeline](https://github.com/CarolinaPB/population-mapping). There are a few `Freebayes` requirements that you need to take into account if you don't use the mapping pipeline mentioned above to map your reads. You should make sure that:
- Alignments have read groups
- Alignments are sorted
- Duplicates are marked

See [here](https://github.com/freebayes/freebayes#calling-variants-from-fastq-to-vcf) for more details. 

#### Tools used

- [Freebayes](https://github.com/freebayes/freebayes) - variant calling using short reads
- [bcftools](https://samtools.github.io/bcftools/bcftools.html) - vcf statistics
- [Plink](https://www.cog-genomics.org/plink/) - compute PCA
- R - Plot PCA

| ![DAG](/workflow.png) |
|:--:|
|*Pipeline workflow* |

### Edit config.yaml with the paths to your files

```yaml
ASSEMBLY: /path/to/ref/fasta_file
MAPPING_DIR: /path/to/bams/dir
GFF_FILE: /path/to/gff_file.gff3 
PREFIX: <prefix> 
OUTDIR: /path/to/outdir
NUM_CHRS: <number of chromosomes>
```

- ASSEMBLY - path to genome fasta file. This file should not be compressed and should be indexed.
  - you can decompress with `gunzip -d <fasta>` and index with `samtools faidx <fasta>`
- MAPPING_DIR - path to directory with bam files to be used
  - the pipeline will use all bam files in the directory, if you want to use a subset of those, create a file named `bam_list.txt` that contains the paths to the bam files you want to use. One path per line.

```text
/path/to/file.bam
/path/to/file2.bam
```
- GFF_FILE - path to gff file. This file should be from the same resource as the genome fasta file. Only Ensembl GFF3 files are supporte by bcftools csq. The script [gff2gff](https://github.com/samtools/bcftools/tree/develop/misc) can help with conversion from non-ensembl GFF formats.
- PREFIX -  prefix for the created files
- OUTDIR - directory where snakemake will run and where the results will be written to  
  If you want the results to be written to this directory (not to a new directory), open config.yaml and comment out `OUTDIR: /path/to/outdir`
- NUM_CHRS - number of chromosomes for your species (necessary for plink). ex: 38

## Additional step
The "scripts/vcf_to_table.py" file contains a script for parsing your VCF file into a table. You can filter your VCF file, for example, to include only "stop_gained" variants and use this as input. The script will then generate a TSV file that includes population frequencies, homozygotes, and heterozygote samples for the variant, as well as information about Consequence, Genes;transcripts (the annotated gene plus its associated transcripts where the variant is present), Protein_coding, Str, AA_sub, Nuc_sub, and Nr transcripts vcf; total nr transcripts gene (indicating the number of transcripts containing the variant and the total number of transcripts for the corresponding gene obtained from the GFF file).
For example: `bcftools view <filename>.csq.vcf.gz| grep -E "stop_gained|#"  > <stop_gained_filtered>.vcf
`


`python scripts/vcf_to_table.py --vcf <filter_vcf>.vcf --gff <gff_file>.gff3.gz --sample <sample_list>.csv --out <out_filename>.tsv
`

sample_list.csv should look like the one for [population-structural-var-calling-smoove-version2](https://git.wur.nl/kim.lensing/population-structural-var-calling-smoove-version2)

## RESULTS

The most important files and directories are:  

- **<run_date>_files.txt** dated file with an overview of the files used to run the pipeline (for documentation purposes)
- **results** directory that contains
  - **final_VCF** directory with variant calling VCF files, as well as VCF stats
    - {prefix}.csq.vcf.gz - final VCF file
    - {prefix}.csq.vcf.gz.stats
  - **PCA** PCA results and plot
    - {prefix}.eigenvec and {prefix}.eigenval - file with PCA eigenvectors and eigenvalues, respectively
    - {prefix}_PCA.pdf - PCA plot

The VCF file has been filtered for `QUAL > 20`. Freebayes is ran with parameters `--use-best-n-alleles 4 --min-base-quality 10 --min-alternate-fraction 0.2 --haplotype-length 0 --ploidy 2 --min-alternate-count 2`. These parameters can be changed in the Snakefile.

The supported consequence types, sorted by impact:

- splice_acceptor_variant .. end region of an intron changed (2bp at the 3' end of an intron)
- splice_donor_variant    .. start region of an intron changed (2bp at the 5' end of an intron)
- stop_gained             .. DNA sequence variant resulting in a stop codon
- frameshift_variant      .. number of inserted/deleted bases not a multiple of three, disrupted translational frame
- stop_lost               .. elongated transcript, stop codon changed
- start_lost              .. the first codon changed
- inframe_altering        .. combination of indels leading to unchanged reading frame and length
- inframe_insertion       .. inserted coding sequence, unchanged reading frame
- inframe_deletion        .. deleted coding sequence, unchanged reading frame
- missense_variant        .. amino acid (aa) change, unchanged length
- splice_region_variant   .. change within 1-3 bases of the exon or 3-8 bases of the intron
- synonymous_variant      .. DNA sequence variant resulting in no amino acid change
- stop_retained_variant   .. different stop codon
- start_retained_variant  .. start codon retained by indel realignment
- non_coding_variant      .. variant in non-coding sequence, such as RNA gene
- 5_prime_UTR_variant
- 3_prime_UTR_variant
- intron_variant          .. reported only if none of the above
- intergenic_variant      .. reported only if none of the above


## Install conda (if it is not yet installed)
Install miniconda on linux. 
[Download installer.](https://docs.conda.io/en/latest/miniconda.html)
[Installation instructions.](https://conda.io/projects/conda/en/latest/user-guide/install/index.html)



1. Download the installer to your home directory. 
Choose the version according to your operating system. You can right click the link, copy and download with:
`wget <link>`

For example:
`wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh`



2. To install miniconda, run:
`bash <installer name>`

For example:
`bash  Miniconda3-latest-Linux-x86_64.sh`



3. Set up the conda channels in this order:
```
conda config --add channels defaults
conda config --add channels bioconda
conda config --add channels conda-forge
```


4. Activate the environment
```
conda activate /lustre/nobackup/WUR/ABGC/shared/pipelines_version2/envs/population-mapping-version2
```
To deactivate the environment (if you want to leave the conda environment)
```
conda deactivate
```

