configfile: "config.yaml"
from snakemake.utils import makedirs

#################################
# author: Carolina Pita Barros  #
# carolina.pitabarros@wur.nl    #
#################################

pipeline = "population-var-calling"

if "OUTDIR" in config:
    workdir: config["OUTDIR"]

makedirs("logs_slurm")

include: "rules/create_file_log.smk"

ASSEMBLY = config["ASSEMBLY"]
MAPPING_DIR = config["MAPPING_DIR"]
GFF_FILE = config["GFF_FILE"]
PREFIX = config["PREFIX"]
NUM_CHRS = config["NUM_CHRS"]

SAMPLES, = glob_wildcards(os.path.join(MAPPING_DIR, "{samples}.sorted.bam"))


localrules: create_bam_list, create_file_log, bcftools_stats, plot_PCA, PCA

rule all:
    input:
        files_log,
        f"results/PCA/{PREFIX}_PCA.pdf",
        f"results/final_VCF/{PREFIX}.csq.vcf.stats",

rule create_bam_list:
    output:
        "bam_list.txt"
    message:
        "Rule {rule} processing"
    params:
        bam_dir = MAPPING_DIR
    shell:
        "ls {params.bam_dir}/*.bam > {output}"


rule var_calling_freebayes:
    input:
        ref = ASSEMBLY,
        bam = rules.create_bam_list.output,
    output:
        vcf = temp("results/variant_calling/{prefix}.vcf.gz"),
        idx = temp("results/variant_calling/{prefix}.vcf.gz.tbi")
    params:
        chunksize = 100000, # reference genome chunk size for parallelization (default: 100000)
        scripts_dir = os.path.join(workflow.basedir, "scripts")
    group:
        'group'
    shell:
        """
        {params.scripts_dir}/freebayes-parallel.sh <({params.scripts_dir}/fasta_generate_regions.py {input.ref}.fai {params.chunksize}) 16 \
        -f {input.ref} \
        --use-best-n-alleles 4 --min-base-quality 10 --min-alternate-fraction 0.2 --haplotype-length 0 --ploidy 2 --min-alternate-count 2 \
        -L {input.bam} | vcffilter -f 'QUAL > 20'| bgzip -c > {output.vcf}
tabix -p vcf {output.vcf}
        """



rule bcftools_csq:
    input:
        vcf = rules.var_calling_freebayes.output.vcf,
        ref = ASSEMBLY, 
        gff = GFF_FILE,
        idx = rules.var_calling_freebayes.output.idx
    output:
        vcf = 'results/final_VCF/{prefix}.csq.vcf.gz',
        idx = 'results/final_VCF/{prefix}.csq.vcf.gz.tbi'
    message:
        'Rule {rule} processing'
    group:
        'group'
    shell:
        """
        bcftools csq --local-csq -O z -f {input.ref} -g {input.gff} {input.vcf} -o {output.vcf} && bcftools index -t {output.vcf} 
        """

rule bcftools_stats:
    input:
        vcf = rules.bcftools_csq.output.vcf,
        idx = rules.bcftools_csq.output.idx
    output:
        "results/final_VCF/{prefix}.csq.vcf.stats"
    message:
        'Rule {rule} processing'
    group:
        'group'
    shell:
        """
        bcftools stats -s - {input.vcf} > {output}
        """


rule PCA:
    input:
        vcf = rules.bcftools_csq.output.vcf,
    output:
        eigenvec = "results/PCA/{prefix}.eigenvec",
        eigenval = "results/PCA/{prefix}.eigenval",
        log = "results/PCA/{prefix}.log",
        nosex = "results/PCA/{prefix}.nosex",
    message:
        'Rule {rule} processing'
    params:
        prefix= os.path.join("results/PCA",PREFIX),
        num_chrs = NUM_CHRS
    group:
        'group'
    shell:
        """
        plink --vcf {input.vcf} --pca --double-id --out {params.prefix} --chr-set {params.num_chrs} --allow-extra-chr --threads 8
        """

rule plot_PCA:
    input:
        eigenvec = rules.PCA.output.eigenvec,
        eigenval = rules.PCA.output.eigenval,
    output:
        "results/PCA/{prefix}_PCA.pdf"
    message:
        'Rule {rule} processing'
    params:
        rscript = os.path.join(workflow.basedir, "scripts/pca.R")
    group:
        'group'
    shell:
        """
        Rscript {params.rscript} {input.eigenvec} {input.eigenval} {output}
        """
